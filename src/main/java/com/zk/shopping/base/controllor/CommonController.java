package com.zk.shopping.base.controllor;

import com.zk.shopping.base.model.KeyValue;
import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.base.model.ReturnCodeEnum;
import com.zk.shopping.base.service.IFileUploadService;
import com.zk.shopping.base.service.ISecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangkai on 3/24/16.
 */

@Controller
@RequestMapping("/")
public class CommonController {

    @Autowired
    private IFileUploadService fileUploadService;

    @Autowired
    private ISecurityService securityService;

    @ResponseBody
    @RequestMapping(value = "upload/img", method = {RequestMethod.POST})
    public ModelResult<String> upload(@RequestBody MultipartFile file) {
        return fileUploadService.upload(file);
    }

    @ResponseBody
    @RequestMapping(value = "rsa/getPublicKey", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelResult<Map<String,Object>> getPublicKey(@RequestParam(required = true) String userId) {
        ModelResult<Map<String,Object>> result = new ModelResult<>("获取公钥成功");
        ModelResult<RSAPublicKey> publicKey = securityService.getRSAPublicKey(userId);
        if(publicKey.getCode() == ReturnCodeEnum.REQUEST_SUCESS.code){
            Map<String,Object> data = new HashMap<>();
            data.put("exponent", publicKey.getData().getPublicExponent().toString(16));
            data.put("modulus", publicKey.getData().getModulus().toString(16));
            result.setData(data);
        }else{
            result.setCode(publicKey.getCode());
            result.setMessage(publicKey.getMessage());
        }
        return result;
    }
}
