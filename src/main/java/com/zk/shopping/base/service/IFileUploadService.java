package com.zk.shopping.base.service;

import com.zk.shopping.base.model.ModelResult;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by zhangkai on 3/30/16.
 */
public interface IFileUploadService {
    ModelResult<String> upload(MultipartFile file);
}
