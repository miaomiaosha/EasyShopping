package com.zk.shopping.base.service;

import com.zk.shopping.base.model.ModelResult;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * 加密相关的服务
 * Created by zhangkai on 3/30/16.
 */
public interface ISecurityService {
    /**
     * 生成公钥，并将signKey对应的公钥
     * @return
     */
    ModelResult<RSAPublicKey> getRSAPublicKey(String signKey);

    /**
     * 根据获取私钥
     * @param signKey
     * @return
     */
    ModelResult<RSAPrivateKey> getRSAPrivateKey(String signKey);

    /**
     *
     * @param signKey 加密键值对的标记
     * @param data 需要加密的数据
     * @return 200-加密成功，1-找不到公钥,2-加密失败
     */
    ModelResult<String> encrypt4RSA(String signKey, String data);
    /**
     *
     * @param signKey 加密键值对的标记
     * @param data 解密的数据
     * @return 200-加密成功，1-找不到私钥,2-解密失败
     */
    ModelResult<String> decrypt4RSA(String signKey, String data);
}
