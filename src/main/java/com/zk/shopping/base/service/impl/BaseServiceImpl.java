package com.zk.shopping.base.service.impl;

import com.zk.shopping.base.dao.IBaseDao;
import com.zk.shopping.base.entity.BaseEntity;
import com.zk.shopping.base.model.ModelPage;
import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.base.service.IBaseService;
import com.zk.shopping.base.utils.ShoppingUtils;
import com.zk.shopping.base.utils.ThreadLocalUtils;
import com.zk.shopping.user.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * Created by zhangkai on 16-2-23.
 */
public class BaseServiceImpl<T extends IBaseDao<E>, E extends BaseEntity> implements IBaseService<T, E> {

    @Autowired
    protected T dao;

    @Override
    public E get(Long id) {
        return dao.selectById(id);
    }

    @Override
    public ModelResult<String> delete(Long id) {
        ModelResult<String> result = new ModelResult<>("删除成功");
        dao.deleteById(id);
        return result;
    }

    @Override
    public ModelResult<E> create(E item) {
        ModelResult<E> result = new ModelResult<>("创建成功");
        handlerCreateUser(item);
        dao.insertSelective(item);
        result.setData(item);
        return result;
    }

    @Override
    public ModelResult<String> modifyById(E item) {
        ModelResult<String> result = new ModelResult<>("修改成功");
        handlerUpdateUser(item);
        dao.updateSelectiveById(item);
        return result;
    }

    @Override
    public ModelPage<E> getPage(Map<String, Object> param) {
        ShoppingUtils.handlerPage(param);
        Integer total = getCount(param);
        List<E> list = getPageList(param);

        ModelPage<E> modelPage = new ModelPage<>();
        modelPage.setPageNo((Integer) param.get("pageNo"));
        modelPage.setPageSize((Integer) param.get("pageSize"));
        modelPage.setItemList(list);
        modelPage.setTotal(total);
        return modelPage;
    }

    @Override
    public Integer getCount(Map<String, Object> param) {
        return dao.countByParams(param);
    }

    @Override
    public List<E> getPageList(Map<String, Object> param) {
        ShoppingUtils.handlerPage(param);
        return dao.listByParams(param);
    }

    @Override
    public List<E> getList(Map<String, Object> param) {
        return dao.listByParams(param);
    }


    protected void handlerUpdateUser(BaseEntity entity) {
        UserEntity loginUser = ThreadLocalUtils.getLoginUserEntity();
        if (loginUser != null) {
            entity.setUpdateAt(System.currentTimeMillis());
            entity.setUpdateBy(loginUser.getUserId());
            entity.setUpdateName(loginUser.getUserName());
        }
    }

    protected void handlerCreateUser(BaseEntity entity) {
        UserEntity loginUser = ThreadLocalUtils.getLoginUserEntity();
        if (loginUser != null) {
            entity.setCreateAt(System.currentTimeMillis());
            entity.setCreateBy(loginUser.getUserId());
            entity.setCreateName(loginUser.getUserName());
        }
    }
}
