package com.zk.shopping.base.utils;

/**
 * Created by zhangkai on 3/31/16.
 */
public class Constants {
    /**
     * 记录连续登陆失败的次数
     */
    public static final String REDIS_LOGIN_ERROR_NUMBER = "login.error.number.";
    /**
     * 限制用户登陆
     */
    public static final String REDIS_LOGIN_ERROR_LIMITED = "login.error.limit.";
    /**
     * RSAkey的PublicExponent
     */
    public static final String REDIS_RSA_KEY_PAIRS_EXPONENT_PUBLIC = "rsa.key.pairs.exponent.public";
    /**
     * RSAkey的PrivateExponent
     */
    public static final String REDIS_RSA_KEY_PAIRS_EXPONENT_PRIVATE = "rsa.key.pairs.exponent.private";
    /**
     * RSAkey的MODULUS
     */
    public static final String REDIS_RSA_KEY_PAIRS_MODULUS = "rsa.key.pairs.modulus";
}
