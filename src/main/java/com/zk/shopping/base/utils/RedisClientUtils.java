package com.zk.shopping.base.utils;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangkai on 3/31/16.
 */
public class RedisClientUtils {

    private static int maxIdle;
    private static int minIdle;
    private static int maxTotal;
    private static int maxWaitMillis;
    private static int timeout;
    private static int weight;
    private static String service1;


    public void setMaxIdle(int maxIdle) {
        RedisClientUtils.maxIdle = maxIdle;
    }

    public void setMinIdle(int minIdle) {
        RedisClientUtils.minIdle = minIdle;
    }

    public void setMaxTotal(int maxTotal) {
        RedisClientUtils.maxTotal = maxTotal;
    }

    public void setMaxWaitMillis(int maxWaitMillis) {
        RedisClientUtils.maxWaitMillis = maxWaitMillis;
    }

    public void setTimeout(int timeout) {
        RedisClientUtils.timeout = timeout;
    }

    public void setWeight(int weight) {
        RedisClientUtils.weight = weight;
    }

    public void setService1(String service1) {
        RedisClientUtils.service1 = service1;
    }

    private static ShardedJedis shardedJedis;//切片额客户端连接
    private static ShardedJedisPool shardedJedisPool;//切片连接池

    public static void init() {
        initialShardedPool();
        shardedJedis = shardedJedisPool.getResource();
    }

    /**
     * 初始化切片池
     */
    private static void initialShardedPool() {
        // 池基本配置
        JedisPoolConfig config = new JedisPoolConfig();
        //设置的逐出策略类名, 默认DefaultEvictionPolicy(当连接超过最大空闲时间,或连接数超过最大空闲连接数)
        config.setEvictionPolicyClassName("org.apache.commons.pool2.impl.DefaultEvictionPolicy");
        //是否启用pool的jmx管理功能, 默认true
        config.setJmxEnabled(true);
        //最大空闲连接数, 默认8个
        config.setMaxIdle(maxIdle);
        //最小空闲连接数, 默认0
        config.setMinIdle(minIdle);
        //最大连接数, 默认8个
        config.setMaxTotal(maxTotal);
        //获取连接时的最大等待毫秒数,如果超时就抛异常
        config.setMaxWaitMillis(maxWaitMillis);
        // slave链接
        List<JedisShardInfo> shards = new ArrayList<>();
        String[] serviceSplit1 = service1.split(":");
        shards.add(new JedisShardInfo(serviceSplit1[0], "r1", Integer.parseInt(serviceSplit1[1]), timeout, weight));
        // 构造池
        shardedJedisPool = new ShardedJedisPool(config, shards);
    }

    /**
     * 新增键值对（不存在会创建，已存在会更新）
     *
     * @param key
     * @param value
     * @return 是否成功
     */
    public static boolean set(String key, String value) {
        return shardedJedis.set(key, value).equals(String.valueOf("OK"));
    }

    /**
     * 新增键值对（不存在会创建，已存在新建失败）
     *
     * @param key
     * @param value
     * @return 是否成功
     */
    public static boolean setnx(String key, String value) {
        return shardedJedis.setnx(key, value).equals(1l);
    }

    /**
     * 新增带有有效期(秒)的键值对（不存在会创建，已存在新建失败）
     *
     * @param key
     * @param seconds 有效时间(秒)
     * @param value
     * @return 是否成功
     */
    public static boolean setex(String key, int seconds, String value) {
        return shardedJedis.setex(key, seconds, value).equals(String.valueOf("OK"));
    }

    public static boolean append(String key, String value) {
        return shardedJedis.append(key, value) > 0;
    }

    /**
     * 获取key对应的值
     *
     * @param key
     * @return
     */
    public static String get(String key) {
        return shardedJedis.get(key);
    }

    /**
     * 增1操作
     *
     * @param key
     * @return
     */
    public static Long incr(String key) {
        return shardedJedis.incr(key);
    }

    /**
     * 增n操作
     *
     * @param key
     * @return
     */
    public static Long incrBy(String key, Long n) {
        return shardedJedis.incrBy(key, n);
    }

    /**
     * 减1操作
     *
     * @param key
     * @return
     */
    public static Long decr(String key) {
        return shardedJedis.decr(key);
    }

    /**
     * 减n操作
     *
     * @param key
     * @return
     */
    public static Long decrBy(String key, Long n) {
        return shardedJedis.decrBy(key, n);
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public static Boolean exists(String key) {
        return shardedJedis.exists(key);
    }

    /**
     * 删除key
     *
     * @param key
     * @return
     */
    public static Long del(String key) {
        return shardedJedis.del(key);
    }

    /**
     * 设置可以的超时时间(秒)
     *
     * @param key
     * @return
     */
    public static Long expire(String key, int seconds) {
        return shardedJedis.expire(key, seconds);
    }

    /**
     * 获取生存时间(秒)
     *
     * @param key
     * @return
     */
    public static int ttl(String key) {
        return shardedJedis.ttl(key).intValue();
    }

}
