package com.zk.shopping.base.utils;

import com.zk.shopping.user.entity.UserEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangkai on 2/26/16.
 */
public class ShoppingUtils {
    /**
     * 处理分页查询，将pageNo（默认1），pageSize（默认15）转换为开始位置（offset）和数量（size）然后存入params
     * @param params
     */
    public static void handlerPage(Map<String, Object> params){
        if(params == null){
            params = new HashMap<>();
        }
        //已经处理过，不必再处理
        if(params.containsKey("offset") ||params.containsKey("size")){
            return;
        }
        //默认值
        if(!params.containsKey("pageNo")){
           params.put("pageNo", 1);
        }
        if(!params.containsKey("pageSize")){
            params.put("pageSize", 15);
        }
        //类型转换
        if(! (params.get("pageNo") instanceof Integer)){
            params.put("pageNo", Integer.parseInt(String.valueOf(params.get("pageNo"))));
        }
        if(! (params.get("pageSize") instanceof Integer)){
            params.put("pageSize", Integer.parseInt(String.valueOf(params.get("pageSize"))));
        }
        //设置参数
        Integer pageNo = (Integer) params.get("pageNo");
        Integer pageSize = (Integer) params.get("pageSize");
        //起始位置（从0开始）
        params.put("offset", (pageNo - 1) * pageSize);
        //数量
        params.put("size", pageSize);
    }

    public static UserEntity getLoginUser(HttpServletRequest request){
        return (UserEntity) request.getSession().getAttribute("userInfo");
    }
}
