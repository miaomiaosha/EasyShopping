package com.zk.shopping.base.utils;

import com.zk.shopping.user.entity.UserEntity;
import nl.bitwalker.useragentutils.UserAgent;

/**
 * Created by Administrator on 16-3-5.
 */
public class ThreadLocalUtils {
    private static final ThreadLocal userSession = new ThreadLocal();

    private static final ThreadLocal deviceSession = new ThreadLocal();

    public static void addLoginUserEntity(UserEntity userEntity) {
        userSession.set(userEntity);
    }

    public static UserEntity getLoginUserEntity() {
        return (UserEntity) userSession.get();
    }

    public static void removeLoginUserEntity() {
        userSession.remove();
    }

    public static void addLoginDevice(UserAgent userAgent) {
        deviceSession.set(userAgent);
    }

    public static UserAgent getLoginDevice() {
        return (UserAgent) deviceSession.get();
    }

    public static void removeLoginDevice() {
        deviceSession.remove();
    }
}
