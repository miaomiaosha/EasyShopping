package com.zk.shopping.menu.controllor;

import com.zk.shopping.base.controllor.BaseControllor;
import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.menu.entity.MenuEntity;
import com.zk.shopping.menu.dao.IMenuDao;
import com.zk.shopping.menu.service.IMenuService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/menu")
public class MenuControllor extends BaseControllor<MenuEntity, IMenuDao, IMenuService> {

    @ResponseBody
    @RequestMapping(value = "delete", method = {RequestMethod.POST})
    public ModelResult<String> delete(@RequestBody Long id) {
        return service.delete(id);
    }

    @ResponseBody
    @RequestMapping(value = "create", method = {RequestMethod.POST})
    public ModelResult<MenuEntity> create(@RequestBody MenuEntity menuEntity) {
        return service.create(menuEntity);
    }

    @ResponseBody
    @RequestMapping(value = "getByRoleIds", method = {RequestMethod.GET})
    public ModelResult<List<MenuEntity>> getByRoleIds(@RequestParam List<Long> roleIds) {
        return service.getByRoleIds(roleIds);
    }

    @ResponseBody
    @RequestMapping(value = "getValidByUserId", method = {RequestMethod.GET})
    public ModelResult<List<MenuEntity>> getValidByUserId(@RequestParam String userId) {
        return service.getValidByUserId(userId);
    }

}
