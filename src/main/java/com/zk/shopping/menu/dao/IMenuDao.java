package com.zk.shopping.menu.dao;


import com.zk.shopping.menu.entity.MenuEntity;
import com.zk.shopping.base.dao.IBaseDao;
/**
 * IMenuDao
*/
public interface IMenuDao extends IBaseDao<MenuEntity>{


}