package com.zk.shopping.menu.entity;

import com.zk.shopping.base.entity.BaseEntity;
import lombok.Data;

@Data
public class MenuEntity  extends BaseEntity {

    /**
    *
    * url(#后面的部分,且不包含#)
    */
    private String url;
    /**
    *
    * 菜单名称
    */
    private String name;
    /**
    *
    * 菜单级别(1表示最高，数字越大，级别越低)
    */
    private Integer level;
    /**
    *
    * 是否是叶子节点(只有叶子节点才有url)[0-否；1-是]
    */
    private Boolean isTail;
    /**
    *
    * 父层菜单ID
    */
    private Long parentId;
    /**
     *
     * 父层菜单名称
     */
    private String parentName;
    /**
    *
    * 状态[0-正常;1-已废除]
    */
    private Integer status;

}
