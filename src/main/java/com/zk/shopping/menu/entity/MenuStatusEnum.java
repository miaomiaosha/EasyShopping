package com.zk.shopping.menu.entity;

/**
 * 菜单状态的枚举
 * Created by zhangkai on 3/8/16.
 */
public enum  MenuStatusEnum {

     //状态[0-正常;1-已废除]
    NORMAL(0,"已启用"), ABOLISH(1,"已废弃");

    public int code;
    public String name;

    MenuStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static MenuStatusEnum getByCode(Integer code){
        for(MenuStatusEnum item : MenuStatusEnum.values()){
            if (item.code == code){
                return item;
            }
        }
        return null;
    }
}
