package com.zk.shopping.menu.service;

import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.menu.entity.MenuEntity;
import com.zk.shopping.menu.dao.IMenuDao;
import com.zk.shopping.base.service.IBaseService;

import java.util.List;


public interface IMenuService  extends IBaseService< IMenuDao, MenuEntity>{

    ModelResult<String> delete(Long id);

    ModelResult<MenuEntity> create(MenuEntity menuEntity);

    ModelResult<List<MenuEntity>> getByRoleIds(List<Long> roleIds);

    /**
     * 根据userId获取用户有效的菜单
     * @param userId
     * @return  [200-成功；1-userId为空；2-用户不存在；]
     */
    ModelResult<List<MenuEntity>> getValidByUserId(String userId);
}
