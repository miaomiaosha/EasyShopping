package com.zk.shopping.menu.service.impl;


import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.base.model.ReturnCodeEnum;
import com.zk.shopping.menu.entity.MenuEntity;
import com.zk.shopping.menu.dao.IMenuDao;
import com.zk.shopping.menu.entity.MenuStatusEnum;
import com.zk.shopping.menu.service.IMenuService;
import com.zk.shopping.base.service.impl.BaseServiceImpl;
import com.zk.shopping.role.entity.RoleEntity;
import com.zk.shopping.role.entity.RoleStatusEnum;
import com.zk.shopping.role.entity.RoleTypeEnum;
import com.zk.shopping.role.service.IRoleService;
import com.zk.shopping.roleMenu.entity.RoleMenuEntity;
import com.zk.shopping.roleMenu.service.IRoleMenuService;
import com.zk.shopping.roleUser.entity.RoleUserEntity;
import com.zk.shopping.roleUser.service.IRoleUserService;
import com.zk.shopping.user.entity.UserEntity;
import com.zk.shopping.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MenuServiceImpl extends BaseServiceImpl<IMenuDao, MenuEntity> implements IMenuService {

    @Autowired
    private IRoleMenuService roleMenuService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IRoleUserService roleUserService;

    @Autowired
    private IUserService userService;

    @Override
    public ModelResult<String> delete(Long id) {
        ModelResult<String> result = new ModelResult<>("删除成功");
        MenuEntity menuInfo = get(id);
        //是否存在
        if(menuInfo == null){
            result.setCode(1);
            result.setMessage("菜单不存在");
            return result;
        }
        //是否已废除
        if(menuInfo.getStatus().equals(MenuStatusEnum.NORMAL.code)){
            result.setCode(2);
            result.setMessage("启用的菜单不允许删除");
            return result;
        }
        //是否有子菜单
        Map<String, Object> reparams = new HashMap<>();
        reparams.put("parentId", id);
        int subNumber = getCount(reparams);
        if(subNumber > 0){
            result.setCode(3);
            result.setMessage("存在子菜单，不允许删除");
            return result;
        }

        super.delete(id);
        return result;
    }

    @Override
    public ModelResult<MenuEntity> create(MenuEntity menuEntity) {
        ModelResult<MenuEntity> result = new ModelResult<>("创建成功");
        super.create(menuEntity);
        result.setData(menuEntity);
        return result;
    }

    @Override
    public ModelResult<List<MenuEntity>> getByRoleIds(List<Long> roleIds) {
        ModelResult<List<MenuEntity>> result = new ModelResult<>("查询成功");
        List<MenuEntity> menuList = new ArrayList<>();
        result.setData(menuList);
        if(CollectionUtils.isEmpty(roleIds)){
            return result;
        }
        Map<String, Object> params = new HashMap<>();
        //是否包含超级管理员
        params.put("idList", roleIds);
        params.put("type", 1);
        int superAdminNumber = roleService.getCount(params);
        params.clear();
        if(superAdminNumber == 0){
            //不包含超级管理
            params.put("roleIdList", roleIds);
            List<RoleMenuEntity> roleMenuList = roleMenuService.getList(params);
            if(CollectionUtils.isEmpty(roleMenuList)){
                return result;
            }
            List<Long> menuIds = new ArrayList<>();
            for(RoleMenuEntity roleMenuEntity : roleMenuList){
                menuIds.add(roleMenuEntity.getMenuId());
            }
            params.clear();
            params.put("idList", menuIds);
            menuList = getList(params);
        }else{
            menuList = getList(params);
        }

        result.setData(menuList);
        return result;
    }

    @Override
    public ModelResult<List<MenuEntity>> getValidByUserId(String userId) {
        ModelResult<List<MenuEntity>>result = new ModelResult<>("查询成功");
        //校验用户
        ModelResult<UserEntity> userInfo = userService.getByUserId(userId);
        if(userInfo.getCode() != ReturnCodeEnum.REQUEST_SUCESS.code){
            result.setCode(userInfo.getCode());
            result.setMessage(userInfo.getMessage());
            return result;
        }
        //获取用户角色
        Map<String ,Object > params = new HashMap<>();
        params.put("userId", userId);
        params.put("status", RoleStatusEnum.RUNNING.code);
        List<RoleUserEntity> roleList = roleUserService.getList(params);
        if(CollectionUtils.isEmpty(roleList)){
            return result;
        }
        List<Long> roleIdList = new ArrayList<>();
        for(RoleUserEntity roleUserEntity : roleList){
            roleIdList.add(roleUserEntity.getRoleId());
        }
        //是否包含超级管理员
        params.put("idList", roleIdList);
        params.put("type", RoleTypeEnum.ADMIN.code);
        params.put("status", RoleStatusEnum.RUNNING.code);
        int superAdminNumber = roleService.getCount(params);
        if(superAdminNumber != 0){
            //超级管理员
            params.clear();
        }else{
            //普通用户
            params.clear();
            params.put("roleIdList", roleIdList);
            List<RoleMenuEntity> roleMenuList = roleMenuService.getList(params);
            if(CollectionUtils.isEmpty(roleMenuList)){
                return result;
            }
            List<Long> menuIds = new ArrayList<>();
            for(RoleMenuEntity roleMenuEntity : roleMenuList){
                menuIds.add(roleMenuEntity.getMenuId());
            }
            params.clear();
            params.put("idList", menuIds);
        }
        params.put("status", MenuStatusEnum.NORMAL.code);
        List<MenuEntity> menuList = getList(params);
        result.setData(menuList);
        return result;
    }
}
