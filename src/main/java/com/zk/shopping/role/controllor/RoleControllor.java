package com.zk.shopping.role.controllor;

import com.zk.shopping.base.controllor.BaseControllor;
import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.role.dao.IRoleDao;
import com.zk.shopping.role.entity.RoleEntity;
import com.zk.shopping.role.service.IRoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/role")
public class RoleControllor extends BaseControllor<RoleEntity, IRoleDao, IRoleService> {

    @ResponseBody
    @RequestMapping(value = "delete", method = {RequestMethod.POST})
    public ModelResult<String> delete(@RequestBody Long id) {
        return service.delete(id);
    }

}
