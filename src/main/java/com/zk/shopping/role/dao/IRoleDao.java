package com.zk.shopping.role.dao;


import com.zk.shopping.role.entity.RoleEntity;
import com.zk.shopping.base.dao.IBaseDao;
/**
 * IRoleDao
*/
public interface IRoleDao extends IBaseDao<RoleEntity>{


}