package com.zk.shopping.role.entity;

import com.zk.shopping.base.entity.BaseEntity;
import lombok.Data;

@Data
public class RoleEntity  extends BaseEntity {

    /**
    *
    * 角色名
    */
    private String name;
    /**
    *
    * 状态[0-已启用;1-已停用]
    */
    private Integer status;
    /**
    *
    * 状态[0-普通角色;1-超级管理员]
    */
    private Integer type;

}
