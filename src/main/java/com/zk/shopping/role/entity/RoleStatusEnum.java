package com.zk.shopping.role.entity;

/**
 * Created by zhangkai on 3/17/16.
 */
public enum RoleStatusEnum {
    //状态[0-已启用;1-已停用]
    RUNNING(0,"已启用"), STOPPED(1,"已停用");

    public int code;
    public String name;

    RoleStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static RoleStatusEnum getByCode(Integer code){
        for(RoleStatusEnum item : RoleStatusEnum.values()){
            if (item.code == code){
                return item;
            }
        }
        return null;
    }
}
