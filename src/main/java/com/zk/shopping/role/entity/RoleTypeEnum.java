package com.zk.shopping.role.entity;

/**
 * Created by zhangkai on 3/17/16.
 */
public enum  RoleTypeEnum {
    //状态[0-普通用户;1-超级管理员]
    COMMON(0,"普通用户"), ADMIN(1,"超级管理员");

    public int code;
    public String name;

    RoleTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static RoleTypeEnum getByCode(Integer code){
        for(RoleTypeEnum item : RoleTypeEnum.values()){
            if (item.code == code){
                return item;
            }
        }
        return null;
    }
}
