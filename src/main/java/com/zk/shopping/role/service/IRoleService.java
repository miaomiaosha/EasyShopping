package com.zk.shopping.role.service;

import com.zk.shopping.role.entity.RoleEntity;
import com.zk.shopping.role.dao.IRoleDao;
import com.zk.shopping.base.service.IBaseService;


public interface IRoleService  extends IBaseService< IRoleDao, RoleEntity>{

}
