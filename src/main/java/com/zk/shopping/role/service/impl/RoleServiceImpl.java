package com.zk.shopping.role.service.impl;


import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.role.entity.RoleEntity;
import com.zk.shopping.role.dao.IRoleDao;
import com.zk.shopping.role.service.IRoleService;
import com.zk.shopping.base.service.impl.BaseServiceImpl;
import com.zk.shopping.roleUser.service.IRoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class RoleServiceImpl extends BaseServiceImpl<IRoleDao, RoleEntity> implements IRoleService {

    @Autowired
    private IRoleUserService roleUserService;

    @Override
    public ModelResult<String> delete(Long id) {
        ModelResult<String> result = new ModelResult<>("删除成功");
        // 校验是否有用户正在使用该角色
        Map<String, Object> reqParams = new HashMap<>();
        reqParams.put("roleId", id);
        int relationSize = roleUserService.getCount(reqParams);
        if(relationSize > 0){
            result.setCode(1);
            result.setMessage("用户正在使用该角色，不允许删除");
            return result;
        }
        return super.delete(id);
    }
}
