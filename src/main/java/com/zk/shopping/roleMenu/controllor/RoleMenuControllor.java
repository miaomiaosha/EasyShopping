package com.zk.shopping.roleMenu.controllor;

import com.zk.shopping.base.controllor.BaseControllor;
import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.roleMenu.dao.IRoleMenuDao;
import com.zk.shopping.roleMenu.entity.RoleMenuEntity;
import com.zk.shopping.roleMenu.service.IRoleMenuService;
import com.zk.shopping.roleMenu.service.params.Request4DistributeMenu;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/roleMenu")
public class RoleMenuControllor extends BaseControllor<RoleMenuEntity, IRoleMenuDao, IRoleMenuService> {

    @ResponseBody
    @RequestMapping("distributeMenu")
    public ModelResult<String> distributeMenu(@RequestBody Request4DistributeMenu request4DistributeMenu){
        return service.distributeMenu(request4DistributeMenu);
    }

}
