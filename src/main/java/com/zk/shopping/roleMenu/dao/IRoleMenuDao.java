package com.zk.shopping.roleMenu.dao;


import com.zk.shopping.base.dao.IBaseDao;
import com.zk.shopping.roleMenu.entity.RoleMenuEntity;

import java.util.List;

/**
 * IRoleMenuDao
*/
public interface IRoleMenuDao extends IBaseDao<RoleMenuEntity>{

    void deleteBatchByIdList(List<Long> idList);

    void insertBatch(List<RoleMenuEntity> entityList);
}