package com.zk.shopping.roleMenu.entity;

import com.zk.shopping.base.entity.BaseEntity;
import lombok.Data;

@Data
public class RoleMenuEntity  extends BaseEntity {

    /**
    *
    * 角色主键
    */
    private Long roleId;
    /**
    *
    * 角色名
    */
    private String roleName;
    /**
    *
    * 菜单主键
    */
    private Long menuId;
    /**
    *
    * 菜单名
    */
    private String menuName;

}
