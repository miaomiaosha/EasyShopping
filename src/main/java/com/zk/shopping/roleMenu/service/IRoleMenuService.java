package com.zk.shopping.roleMenu.service;

import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.base.service.IBaseService;
import com.zk.shopping.roleMenu.dao.IRoleMenuDao;
import com.zk.shopping.roleMenu.entity.RoleMenuEntity;
import com.zk.shopping.roleMenu.service.params.Request4DistributeMenu;

import java.util.List;


public interface IRoleMenuService  extends IBaseService< IRoleMenuDao, RoleMenuEntity>{

    ModelResult<String> deleteBatch(List<Long> idList);
    ModelResult<String> createBatch(List<RoleMenuEntity> entityList);
    ModelResult<String> distributeMenu(Request4DistributeMenu request4DistributeMenu);
}
