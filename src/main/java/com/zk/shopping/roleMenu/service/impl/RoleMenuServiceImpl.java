package com.zk.shopping.roleMenu.service.impl;


import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.menu.entity.MenuEntity;
import com.zk.shopping.role.entity.RoleEntity;
import com.zk.shopping.role.service.IRoleService;
import com.zk.shopping.roleMenu.entity.RoleMenuEntity;
import com.zk.shopping.roleMenu.dao.IRoleMenuDao;
import com.zk.shopping.roleMenu.service.IRoleMenuService;
import com.zk.shopping.base.service.impl.BaseServiceImpl;
import com.zk.shopping.roleMenu.service.params.Request4DistributeMenu;
import com.zk.shopping.roleUser.entity.RoleUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RoleMenuServiceImpl extends BaseServiceImpl<IRoleMenuDao, RoleMenuEntity> implements IRoleMenuService {

    @Autowired
    private IRoleService roleService;

    @Override
    public ModelResult<String> deleteBatch(List<Long> idList) {
        ModelResult<String> result = new ModelResult<>("删除成功");
        dao.deleteBatchByIdList(idList);
        return result;
    }

    @Override
    public ModelResult<String> createBatch(List<RoleMenuEntity> entityList) {
        ModelResult<String> result = new ModelResult<>("新增成功");
        for(RoleMenuEntity entity : entityList){
            handlerCreateUser(entity);
        }
        dao.insertBatch(entityList);
        return result;
    }

    // code :2-角色不存在
    @Override
    public ModelResult<String> distributeMenu(Request4DistributeMenu request4DistributeMenu) {
        ModelResult<String> result = new ModelResult<>("新增成功");
        RoleEntity roleEntity = roleService.get(request4DistributeMenu.getRoleId());
        if(roleEntity == null){
            result.setCode(1);
            result.setMessage("角色不存在");
            return result;
        }
        Map<String, Object> params = new HashMap<>();
        params.put("roleId", roleEntity.getId());
        List<RoleMenuEntity> orgRoleMenuList = getList(params);
        //删除菜单
        List<Long> deleteIds = new ArrayList<>();
        for(RoleMenuEntity roleMenuEntity : orgRoleMenuList){
            boolean isDelete = true;
            for(MenuEntity menu : request4DistributeMenu.getMenuList()){
                if(menu.getId().equals(roleMenuEntity.getMenuId())){
                    isDelete = false;
                    break;
                }
            }
            if(isDelete){
                deleteIds.add(roleMenuEntity.getId());
            }
        }
        if(!CollectionUtils.isEmpty(deleteIds)){
            deleteBatch(deleteIds);
        }
        //新增的
        List<RoleMenuEntity> createList = new ArrayList<>();
        for(MenuEntity menu : request4DistributeMenu.getMenuList()){
            boolean isNew = true;
            for(RoleMenuEntity roleMenuEntity : orgRoleMenuList) {
                if (menu.getId().equals(roleMenuEntity.getMenuId())) {
                    isNew = false;
                    break;
                }
            }
            if(isNew){
                RoleMenuEntity roleMenuEntity = new RoleMenuEntity();
                roleMenuEntity.setRoleId(roleEntity.getId());
                roleMenuEntity.setRoleName(roleEntity.getName());
                roleMenuEntity.setMenuId(menu.getId());
                roleMenuEntity.setMenuName(menu.getName());
                roleMenuEntity.setIsDel(false);
                createList.add(roleMenuEntity);
            }
        }
        if(!CollectionUtils.isEmpty(createList)){
            createBatch(createList);
        }
        return result;
    }
}
