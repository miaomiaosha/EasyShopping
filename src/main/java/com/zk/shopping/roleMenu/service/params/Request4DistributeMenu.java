package com.zk.shopping.roleMenu.service.params;

import com.zk.shopping.menu.entity.MenuEntity;
import lombok.Data;

import java.util.List;

/**
 * Created by zhangkai on 3/16/16.
 */
@Data
public class Request4DistributeMenu {
    private Long roleId;
    private String roleName;
    private List<MenuEntity> menuList;
}
