package com.zk.shopping.roleUser.controllor;

import com.zk.shopping.base.controllor.BaseControllor;
import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.roleUser.entity.RoleUserEntity;
import com.zk.shopping.roleUser.dao.IRoleUserDao;
import com.zk.shopping.roleUser.service.IRoleUserService;
import com.zk.shopping.roleUser.service.params.Requset4Authority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/roleUser")
public class RoleUserControllor extends BaseControllor<RoleUserEntity, IRoleUserDao, IRoleUserService> {


    @ResponseBody
    @RequestMapping(value = "authority", method = {RequestMethod.POST})
    public ModelResult<String> authority(@RequestBody Requset4Authority requset4Authority) {
        ModelResult<String> result = service.authority(requset4Authority);
        return result;
    }
}
