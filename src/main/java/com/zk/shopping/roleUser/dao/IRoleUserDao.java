package com.zk.shopping.roleUser.dao;


import com.zk.shopping.roleUser.entity.RoleUserEntity;
import com.zk.shopping.base.dao.IBaseDao;

import java.util.List;

/**
 * IRoleUserDao
*/
public interface IRoleUserDao extends IBaseDao<RoleUserEntity>{


    void deleteBatchByIdList(List<Long> idList);

    void insertBatch(List<RoleUserEntity> entityList);
}