package com.zk.shopping.roleUser.entity;

import com.zk.shopping.base.entity.BaseEntity;
import lombok.Data;

@Data
public class RoleUserEntity  extends BaseEntity {

    /**
    *
    * 角色主键
    */
    private Long roleId;
    /**
    *
    * 角色名
    */
    private String roleName;
    /**
    *
    * 用户ID
    */
    private String userId;
    /**
    *
    * 真实姓名
    */
    private String userName;

}
