package com.zk.shopping.roleUser.service;

import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.roleUser.entity.RoleUserEntity;
import com.zk.shopping.roleUser.dao.IRoleUserDao;
import com.zk.shopping.base.service.IBaseService;
import com.zk.shopping.roleUser.service.params.Requset4Authority;

import java.util.List;


public interface IRoleUserService  extends IBaseService< IRoleUserDao, RoleUserEntity>{
    List<RoleUserEntity> getByUserId(String userId);
    ModelResult<String> deleteBatch(List<Long> idList);
    ModelResult<String> createBatch(List<RoleUserEntity> entityList);

    ModelResult<String> authority(Requset4Authority requset4Authority);
}
