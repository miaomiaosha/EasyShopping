package com.zk.shopping.roleUser.service.impl;


import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.base.model.ReturnCodeEnum;
import com.zk.shopping.base.service.impl.BaseServiceImpl;
import com.zk.shopping.role.entity.RoleEntity;
import com.zk.shopping.roleUser.dao.IRoleUserDao;
import com.zk.shopping.roleUser.entity.RoleUserEntity;
import com.zk.shopping.roleUser.service.IRoleUserService;
import com.zk.shopping.roleUser.service.params.Requset4Authority;
import com.zk.shopping.user.entity.UserEntity;
import com.zk.shopping.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RoleUserServiceImpl extends BaseServiceImpl<IRoleUserDao, RoleUserEntity> implements IRoleUserService {

    @Autowired
    private IUserService userService;

    @Override
    public List<RoleUserEntity> getByUserId(String userId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        return getList(params);
    }


    @Override
    public ModelResult<String> deleteBatch(List<Long> idList) {
        ModelResult<String> result = new ModelResult<>("删除成功");
        dao.deleteBatchByIdList(idList);
        return result;
    }

    @Override
    public ModelResult<String> createBatch(List<RoleUserEntity> entityList) {
        ModelResult<String> result = new ModelResult<>("新增成功");
        for(RoleUserEntity entity : entityList){
            handlerCreateUser(entity);
        }
        dao.insertBatch(entityList);
        return result;
    }

    @Override
    public ModelResult<String> authority(Requset4Authority requset4Authority) {
        ModelResult<String> result = new ModelResult<>("修改成功");
        ModelResult<UserEntity> userEntityModelReslt = userService.getByUserId(requset4Authority.getUserId());
        if (userEntityModelReslt.getCode() != ReturnCodeEnum.REQUEST_SUCESS.code) {
            result.setCode(userEntityModelReslt.getCode());
            result.setMessage(userEntityModelReslt.getMessage());
            return result;
        }
        List<RoleUserEntity> orginRoleUserList = getByUserId(requset4Authority.getUserId());
        //删除的
        List<Long> removeRoleUserIdList = new ArrayList<>();
        for(RoleUserEntity orRole : orginRoleUserList){
            boolean isRemove = true;
            for(RoleEntity newRole : requset4Authority.getRoleList()){
                if(orRole.getRoleId().equals(newRole.getId())){
                    isRemove = false;
                    break;
                }
            }
            if(isRemove){
                removeRoleUserIdList.add(orRole.getId());
            }
        }
        if(!CollectionUtils.isEmpty(removeRoleUserIdList)){
            deleteBatch(removeRoleUserIdList);
        }
        //新增的
        List<RoleUserEntity> addRoleUserList = new ArrayList<>();
        for(RoleEntity orRole : requset4Authority.getRoleList()){
            boolean isAdd = true;
            for(RoleUserEntity newRole : orginRoleUserList){
                if(orRole.getId().equals(newRole.getRoleId())){
                    isAdd = false;
                    break;
                }
            }
            if(isAdd){
                RoleUserEntity roleUserEntity = new RoleUserEntity();
                roleUserEntity.setUserId(requset4Authority.getUserId());
                roleUserEntity.setUserName(requset4Authority.getUserName());
                roleUserEntity.setRoleId(orRole.getId());
                roleUserEntity.setRoleName(orRole.getName());
                roleUserEntity.setIsDel(false);
                addRoleUserList.add(roleUserEntity);
            }
        }
        if(!CollectionUtils.isEmpty(addRoleUserList)){
            createBatch(addRoleUserList);
        }
        return result;
    }

}
