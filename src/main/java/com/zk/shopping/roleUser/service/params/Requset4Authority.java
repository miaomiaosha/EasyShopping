package com.zk.shopping.roleUser.service.params;

import com.zk.shopping.role.entity.RoleEntity;
import lombok.Data;

import java.util.List;

/**
 * 用户授权的请求参数
 * Created by zhangkai on 3/15/16.
 */
@Data
public class Requset4Authority {
    private String userId;
    private String userName;
    private List<RoleEntity> roleList;
}
