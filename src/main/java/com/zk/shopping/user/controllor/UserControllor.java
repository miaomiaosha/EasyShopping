package com.zk.shopping.user.controllor;

import com.zk.shopping.base.controllor.BaseControllor;
import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.user.dao.IUserDao;
import com.zk.shopping.user.entity.UserEntity;
import com.zk.shopping.user.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
public class UserControllor extends BaseControllor<UserEntity, IUserDao, IUserService> {


    @ResponseBody
    @RequestMapping("login")
    public ModelResult<UserEntity> login(@RequestParam(required = true) String userId,
                                         @RequestParam(required = true) String password) {
        ModelResult<UserEntity> result = service.login(this.request, userId, password);
        return result;
    }

    @ResponseBody
    @RequestMapping("logout")
    public ModelResult<String> login() {
        ModelResult<String> result = service.logout(this.request);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "changeStatus", method = {RequestMethod.POST})
    public ModelResult<String> changeStatus(@RequestBody UserEntity userEntity) {
        String userId = userEntity.getUserId();
        Integer status = userEntity.getStatus();
        ModelResult<String> result = service.changeStatus(userId, status);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "update", method = {RequestMethod.POST})
    public ModelResult<String> update(@RequestBody UserEntity userEntity) {
        ModelResult<String> result = service.update(userEntity);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "create", method = {RequestMethod.POST})
    public ModelResult<UserEntity> create(@RequestBody UserEntity userEntity) {
        ModelResult<UserEntity> result = service.create(userEntity);
        return result;
    }

}
