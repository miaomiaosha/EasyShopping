package com.zk.shopping.user.dao;


import com.zk.shopping.user.entity.UserEntity;
import com.zk.shopping.base.dao.IBaseDao;
/**
 * IUserDao
*/
public interface IUserDao extends IBaseDao<UserEntity>{


}