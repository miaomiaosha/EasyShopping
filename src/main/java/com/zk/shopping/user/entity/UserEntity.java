package com.zk.shopping.user.entity;

import com.zk.shopping.base.entity.BaseEntity;
import lombok.Data;

@Data
public class UserEntity  extends BaseEntity {

    /**
    *
    * 用户ID
    */
    private String userId;
    /**
    *
    * 真实姓名
    */
    private String userName;
    /**
    *
    * 用户昵称
    */
    private String nickName;
    /**
    *
    * 用户手机号
    */
    private String userMobile;
    /**
    *
    * 用户性别[0-保密，1-男，2-女]
    */
    private Integer userSex;
    /**
     *
     * 出生年月日
     */
    private Long userBirth;
    /**
     *
     * 用户头像
     */
    private String imgUrl;
    /**
    *
    * 密码
    */
    private String password;
    /**
    *
    * 状态[0-正常;1-冻结]
    */
    private Integer status;

}
