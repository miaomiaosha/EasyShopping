package com.zk.shopping.user.entity;

/**
 * 用户状态枚举
 * Created by zhangkai on 2/29/16.
 */
public enum  UserStatusEnum {
    NORMAL(0,"正常"), FROZEN(1,"冻结");

    public int code;
    public String name;

    UserStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static UserStatusEnum getByCode(Integer code){
        for(UserStatusEnum item : UserStatusEnum.values()){
            if (item.code == code){
                return item;
            }
        }
        return null;
    }

}
