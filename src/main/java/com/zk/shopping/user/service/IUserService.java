package com.zk.shopping.user.service;

import com.zk.shopping.base.model.ModelResult;
import com.zk.shopping.user.entity.UserEntity;
import com.zk.shopping.user.dao.IUserDao;
import com.zk.shopping.base.service.IBaseService;

import javax.servlet.http.HttpServletRequest;


public interface IUserService  extends IBaseService< IUserDao, UserEntity>{

    ModelResult<UserEntity> login(HttpServletRequest request, String userId, String password);
    ModelResult<String> logout(HttpServletRequest request);

    ModelResult<UserEntity> getByUserId(String userId);

    ModelResult<String> changeStatus(String userId, Integer status);

    ModelResult<String> update(UserEntity userEntity);
}
