package com.zk.shopping.userLog.controllor;

import com.zk.shopping.base.controllor.BaseControllor;
import com.zk.shopping.userLog.entity.UserLogEntity;
import com.zk.shopping.userLog.dao.IUserLogDao;
import com.zk.shopping.userLog.service.IUserLogService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/userLog")
public class UserLogControllor extends BaseControllor<UserLogEntity, IUserLogDao, IUserLogService> {


}
