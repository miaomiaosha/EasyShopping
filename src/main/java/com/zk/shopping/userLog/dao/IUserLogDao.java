package com.zk.shopping.userLog.dao;


import com.zk.shopping.userLog.entity.UserLogEntity;
import com.zk.shopping.base.dao.IBaseDao;
/**
 * IUserLogDao
*/
public interface IUserLogDao extends IBaseDao<UserLogEntity>{


}