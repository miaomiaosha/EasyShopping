package com.zk.shopping.userLog.entity;

import com.zk.shopping.base.entity.BaseEntity;
import lombok.Data;

@Data
public class UserLogEntity  extends BaseEntity {

    /**
     *
     * 用户ID
     */
    private String userId;

    /**
     *
     * 用户名
     */
    private String userName;

    /**
    *
    * 日志内容
    */
    private String content;
    /**
    *
    * 状态[0-创建;1-修改；2-修改密码；4-登陆]
    */
    private Integer type;

}
