package com.zk.shopping.userLog.entity;

/**
 * Created by zhangkai on 2/29/16.
 */
public enum UserLogTypeEnum {
    CREATE(0, "创建用户"), UPDATE(1, "修改用户信息"), CHANGEPASSWD(2, "修改密码"), LOGIN(4, "登陆");
    public int code;
    public String name;

    UserLogTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static UserLogTypeEnum getByCode(Integer code){
        for(UserLogTypeEnum item : UserLogTypeEnum.values()){
            if (item.code == code){
                return item;
            }
        }
        return null;
    }

}
