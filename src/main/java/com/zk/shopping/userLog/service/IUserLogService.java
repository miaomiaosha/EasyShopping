package com.zk.shopping.userLog.service;

import com.zk.shopping.base.service.IBaseService;
import com.zk.shopping.userLog.dao.IUserLogDao;
import com.zk.shopping.userLog.entity.UserLogEntity;
import com.zk.shopping.userLog.entity.UserLogTypeEnum;

public interface IUserLogService extends IBaseService<IUserLogDao, UserLogEntity> {

    void save(String content, String userId, String userName, UserLogTypeEnum logType);
}
