package com.zk.shopping.userLog.service.impl;


import com.zk.shopping.base.service.impl.BaseServiceImpl;
import com.zk.shopping.base.utils.ThreadLocalUtils;
import com.zk.shopping.user.entity.UserEntity;
import com.zk.shopping.userLog.dao.IUserLogDao;
import com.zk.shopping.userLog.entity.UserLogEntity;
import com.zk.shopping.userLog.entity.UserLogTypeEnum;
import com.zk.shopping.userLog.service.IUserLogService;
import nl.bitwalker.useragentutils.UserAgent;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserLogServiceImpl extends BaseServiceImpl<IUserLogDao, UserLogEntity> implements IUserLogService {



    /**
     * 记录登陆日志
     * @param content 日志内容
     * @param userId 用户ID
     */
    @Override
    public void save(String content, String userId, String userName, UserLogTypeEnum logType){
        UserAgent userAgent = ThreadLocalUtils.getLoginDevice();
        UserLogEntity userLogEntity = new UserLogEntity();
        userLogEntity.setType(logType.code);
        if(userAgent != null) {
            String osInfo = userAgent.getOperatingSystem().getName() + "," + userAgent.getBrowser().getName();
            content = content + "[" + osInfo + "]";
        }
        userLogEntity.setContent(content);
        userLogEntity.setUserId(userId);
        userLogEntity.setUserName(userName);
        //未登录
        if(ThreadLocalUtils.getLoginUserEntity() == null){
            userLogEntity.setCreateAt(System.currentTimeMillis());
            userLogEntity.setCreateBy(userId);
            userLogEntity.setCreateName(userName);
        }
        create(userLogEntity);
    }
}
